package com.diffblue.ping;

import java.util.HashMap;

/**
 * Created by darrenroyle on 13/02/2018.
 */
public class Pong {
    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
    public static int valuesIterate() {
        HashMap<Integer,Integer> hashmap = new HashMap<Integer,Integer>();
        hashmap.put(12, 34);
        hashmap.put(56, 78);
        int result = 0;
        for (Integer i : hashmap.values()) {
            result += i;
        }
        return result;
    }
}

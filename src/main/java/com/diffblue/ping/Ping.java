package com.diffblue.ping;

import java.lang.String;
import java.math.BigInteger;
import static shiver.me.timbers.data.random.RandomBigIntegers.someBigInteger;

public class Ping {
    public String checkPing(String inp) {
        if(inp.equals("ping")) {
            return "pong";
        } else {
            return "ping";
        }
    }
    public String checkPants(String inp) {
        if(inp.equals("pants")) {
            return "IsPants";
        } else {
            return "IsNotPants";
        }
    }
    public String checkPong(String inp) {
        if(inp.equals("pong")) {
            return "ping";
        } else {
            return "pong";
        }
    }
    public BigInteger getRandomPing() {
        return someBigInteger();
    }
}